module.exports = (temp, product) => {
    const {image, organic, productName, description, from, nutrients, quantity, id, price} = product;
    let output = temp.replace(/{%PRODUCT-NAME%}/g, productName);
    output = output.replace(/{%PRODUCT-IMAGE%}/g, image);
    output = output.replace(/{%PRODUCT-FROM%}/g, from);
    output = output.replace(/{%PRODUCT-PRICE%}/g, price);
    output = output.replace(/{%PRODUCT-NUTRIENTS%}/g, nutrients);
    output = output.replace(/{%PRODUCT-QUANTITY%}/g, quantity);
    output = output.replace(/{%PRODUCT-DESCRIPTION%}/g, description);
    output = output.replace(/{%PRODUCT-ID%}/g, id);
    if (!organic) output = output.replace(/{%PRODUCT-NOT-ORGANIC%}/g, 'not-organic');
    return output;
};
