const fs = require('fs');
const http = require('http');
const url = require('url');
const replaceTemplate = require('./Modules/replaceTemplate');
// synchronous
/*const input = fs.readFileSync('./txt/input.txt','utf-8')
console.log(input);

const textOut = `Lorem ipsum dolor sit amet,ver ${input}`
fs.writeFileSync('./txt/output.txt',textOut,'utf-8')*/

// async way
/*fs.readFile('./txt/start.txt','utf-8', (err, data)=>{
    if (err) return
    fs.readFile(`./txt/${data}.txt`,'utf-8',(err, data2)=>{
        console.log(data2);
        fs.readFile('./txt/append.txt','utf-8',(err, data3)=>{
            console.log(data3);
            fs.writeFile('./txt/final.txt', `${data2} \n ${data3}`,(err)=>{
                console.log(err,'written');
            })
        })
    })
})*/

// ============================================
// Server

const productJson = fs.readFileSync(`${__dirname}/Dev/data.json`, 'utf-8');
const productObj = JSON.parse(productJson);
const slugs = productObj.map(el =>
  el.productName.replace(/ /g, '-').toLowerCase()
);

const templateOverview = fs.readFileSync(
  `${__dirname}/templates/template-overview.html`,
  'utf-8'
);
const templateCard = fs.readFileSync(
  `${__dirname}/templates/template-card.html`,
  'utf-8'
);
const templateProduct = fs.readFileSync(
  `${__dirname}/templates/template-product.html`,
  'utf-8'
);

const server = http.createServer((req, res) => {
  const { query, pathname } = url.parse(req.url, true);

  // overview page
  if (pathname === '/overview' || pathname === '/') {
    res.writeHead(200, { 'Content-type': 'text/html' });
    const cardHTML = productObj
      .map(card => replaceTemplate(templateCard, card))
      .join('');
    const outputOverview = templateOverview.replace(
      /{%PRODUCT-CARDS%/g,
      cardHTML
    );
    res.end(outputOverview);

    //     api
  } else if (pathname === '/api') {
    res.writeHead(200, {
      'Content-Type': 'application/json',
    });
    res.end(productJson);
    //     product
  } else if (pathname === '/product') {
    const product = productObj[query.id];
    res.writeHead(200, { 'Content-type': 'text/html' });
    let outputProduct = replaceTemplate(templateProduct, product);
    res.end(outputProduct);
  }
  //     not found
  else {
    res.writeHead(404);
    res.end('nothing to show');
  }
});

// server.
server.listen(8000, '127.0.0.1', () => console.log('listen to me via 8000'));
