const fs = require('fs');
const server = require('http').createServer();
server.on('request', (req, res) => {
  //   1
  /* fs.readFile('./text.txt', 'utf-8', (err, data) => {
    if (err) console.log(err);
    res.end(data);
  });*/
  //   2
  /* const readable = fs.createReadStream('./text.txt');
  readable.on('data', chunk => {
    console.log(chunk);
    res.write(chunk);
  });
  readable.on('end', () => {
    res.end();
  });
  readable.on('error', err => {
    console.log(err);
    res.statusCode = 500;
    res.end('file not found');
  });*/
  //   3
  const readable = fs.createReadStream('./text.txt');
  readable.pipe(res);
});

server.listen(8000, '127.0.0.1', () =>
  console.log('server is running on port 8000')
);
