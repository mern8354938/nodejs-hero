const EventEmitter = require('events');
const http = require('http');
class Call extends EventEmitter {
  constructor(props) {
    super(props);
  }
}

const emitter = new Call();
emitter.on('call', () => console.log('calling...'));
emitter.on('call', () => console.log('from su calling...'));

emitter.on('call', name => {
  console.log(`name is ${name}`);
});
emitter.emit('call', 'jon');

///////////////000000000
const server = http.createServer();

server.on('request', (req, res) => {
  console.log('req received');
  res.end('request accepted');
});
server.on('request', (req, res) => {
  console.log('request accepted');
});
server.on('close', () => {
  console.log('closed ...');
});
server.listen(8000, '127.0.0.1', () => console.log('running on port 8000'));
