const fs = require('fs');
const crypto = require('crypto');
const start = Date.now();

setTimeout(() => {
  console.log('timer 1');
}, 0);
setImmediate(() => console.log('immediate 1'));
fs.readFile('./text.txt', 'utf-8', (err, data) => {
  console.log('I/o finished');
  console.log('/////////////////////');

  setTimeout(() => console.log('timer 2'), 0);
  setTimeout(() => console.log('timer 3'), 3000);
  setImmediate(() => console.log('immediate 2'));

  process.nextTick(() => console.log('process tick'));

  crypto.pbkdf2('jondoe', 'name', 100000, 1024, 'sha512', () => {
    console.log(Date.now() - start, 'password encrypted');
  });
  crypto.pbkdf2('jondoe', 'name', 10000, 1024, 'sha512', () => {
    console.log(Date.now() - start, 'password encrypted');
  });
  crypto.pbkdf2('jondoe', 'name', 10000, 1024, 'sha512', () => {
    console.log(Date.now() - start, 'password encrypted');
  });
  crypto.pbkdf2('jondoe', 'name', 10000, 1024, 'sha512', (err, derivedKey) => {
    console.log(Date.now() - start, 'password encrypted');
  });
});

console.log('top level');
