const mongoose = require('mongoose');
const fs = require('fs');
const https = require('https');
const dotenv = require('dotenv');
dotenv.config({
  path: `${__dirname}/config.env`,
});
const app = require('./app');

const DB = process.env.DATABASE.replace('<PASSWORD>', process.env.PASSWORD);
console.log(DB);
mongoose
  .connect(DB)
  .then(() => console.log('success'))
  .catch((err) => console.log(err.message));

// const options = {
//   key: fs.readFileSync(__dirname + '/key.pem'),
//   cert: fs.readFileSync(__dirname + '/cert.pem'),
// };
// const server = https.createServer(options, app);
PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`server running on PORT ${PORT} `));
