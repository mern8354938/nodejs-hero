const mongoose = require('mongoose')
const validator = require('validator')
const userSchema =new mongoose.Schema({
    name:{
        type:String,
        required:[true,'a user must have name lol'],
        trim:true,
        maxlength: [22,'a name not exceed 22 characters']
        ,minlength: [3,'a name must contain 3 characters']
    },
    email:{
        type:String,
        required: [true,'a user must have email yeh..']
        ,trim:true,
        unique:true
        ,lowercase:true,
        validate:[validator.isEmail,'use a valid email address']
    }
    , photo:{
        type:String
    },
    password:{
        type:String,
        trim:true,
        required:[true,'a user must have password lol'],
        maxlength: [32,'a password not exceed 22 characters']
        ,minlength: [8,'a password must contain 3 characters'],
        validate:[validator.isStrongPassword,'enter a diamond password']
    },
    passwordConfirm:{
        type:String,
        required:[true,'a user must have password lol'],
        maxlength: [32,'a password not exceed 22 characters']
        ,minlength: [8,'a password must contain 3 characters'],
        validate:[validator.isStrongPassword,'enter a diamond password']

    }
})

const User = mongoose.model('User',userSchema)

module.exports = User
