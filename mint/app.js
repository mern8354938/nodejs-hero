// noinspection JSValidateTypes

const e = require('express');
const express = require('express');
const morgan = require('morgan');

const AppError = require(`${__dirname}/Utils/appError`);
const globalErrorHandler = require(`${__dirname}/Controllers/errorController`);
const tourRouter = require(`${__dirname}/Routes/tourRoutes`);
const userRouter = require(`${__dirname}/Routes/userRoutes`);

const API_TOUR_ENDPOINT = '/api/v1/tours';
const API_USER_ENDPOINT = '/api/v1/users';

const app = express();

app.use(express.json());
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}
app.use(express.static(`${__dirname}/public`));
app.use((req, res, next) => {
  req.requestTime = new Date().toISOString();
  next();
});

// -> ROUTES
app.use(API_TOUR_ENDPOINT, tourRouter);
app.use(API_USER_ENDPOINT, userRouter);

app.all('*', (req, res, next) => {
  next(new AppError(`Cant find ${req.originalUrl} on the server..`), 404);
});

app.use(globalErrorHandler);

module.exports = app;
