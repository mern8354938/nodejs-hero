const User = require ('../Models/userModal')

exports.signup = async (req,res,next)=>{
    try {
    const newUser = await User.create(req.body)

    res.status(201).json({
        status:'success',
        data:{user:newUser}
    })}
    catch (e) {
        console.log(e.message)
    }
}
