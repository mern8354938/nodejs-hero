const express = require('express');
const {
  getAllUsers,
  createUser,
  getUser,
  updateUser,
  deleteUser,
} = require(`${__dirname}/../Controllers/userController.js`);
const {signup} = require(`${__dirname}/../Controllers/authController.js`)

const router = express.Router();
router.post('/signup',signup)

router.route('/').get(getAllUsers).post(createUser);
router.route('/:id').get(getUser).patch(updateUser).delete(deleteUser);

module.exports = router;
