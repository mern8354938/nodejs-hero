
const fs = require('fs');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const Tour = require('../../Models/tourModel');

dotenv  .config({path:`${__dirname}/../../config.env`})
// Dev/data/import-dev-data.js

const DB = process.env.DATABASE.replace('<PASSWORD>',process.env.PASSWORD)

mongoose.connect(DB).then(()=>console.log('success'))
const tours =JSON.parse( fs.readFileSync((`${__dirname}/tours-simple.json`),'utf-8'))

const importData = async () => {
  try {
    await Tour.create(tours)
    console.log('data loaded');
  }catch (e) {
    console.log(e);
  }
}
//delete data from collection
const deleteData = async () =>{
  try {
    await Tour.deleteMany()
    console.log('data deleted');
  }catch (e) {
    console.log(e);
  }
}

if(process.argv[2] === '--import'){
  importData()
  // process.exit(0)
}else if(process.argv[2] === '--delete'){
  deleteData()
  // process.exit(0)
}
console.log(process.argv);